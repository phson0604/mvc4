﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Xunit;




namespace MVC.fluentassert
{
    public class fluentTest
    {
        [Fact]
        public void TestAddCorrect()
        {
            string username = "denni";
            username.Should().Be("denni");
        }
        [Fact]
        public void TestAddInCorrect()
        {
            string username = "denni";
            username.Should().Be("denni1");
        }
        int Add(int x, int y)
        {
            return x + y;
        }

        [Fact]
        void FactTest()
        {
            Add(2, 2).Should().Be(4);
        }

        [Theory]
        [InlineData(2, 3, 5)]
        [InlineData(3, 3, 7)]
        void TheoryTest(int a, int b, int sum)
        {
            sum.Should().Be(Add(a, b));
        }
       
    }
  
}
