﻿using FluentValidation;
using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVC.Validators
{
    public class StudentValidation :AbstractValidator<LsClass>
    {
        public StudentValidation()
        {
            RuleFor(p => p.NameStudent).NotEmpty().WithMessage("sTUDENT Name is required ");
            RuleFor(p => p.DateStudent)
                .Must(BeAvalidAge).WithMessage("invalid {Propetyname}");
        }
        protected bool BeAvalidAge(DateTime date)
        {
            int currentYear = DateTime.Now.Year;
            int dobYear = date.Year;
            if (dobYear <= (currentYear - 6))
            {
                return true;
            }
            return false;
        }
    }
}
