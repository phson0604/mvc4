﻿using FluentValidation;
using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVC.Validators
{
    public class PersonValidator : AbstractValidator<Teacher>
    {
        public PersonValidator()
        {
            RuleFor(p => p.TeacherName).NotEmpty().WithMessage("Teacher Name is required ");
            RuleFor(p => p.TeacherDate)
                .Must(BeAvalidAge).WithMessage("invalid {Propetyname}");
           
        }

        

        protected bool BeAvalidAge(DateTime date)
        {
            int currentYear = DateTime.Now.Year;
            int dobYear = date.Year;
            if(dobYear <= (currentYear-20))
            {
                return true;
            }
            return false;
        }
    }
}
