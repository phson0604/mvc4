﻿   using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using MVC.Models;
namespace MVC.Controllers
{
    class Ctrl_Score
    {
        Ctrl_Student student = new Ctrl_Student();
        public void cmb_class(ComboBox x)
        {
            student.cbb_classes(x);

        }
        public void cmb_year(ComboBox x)
        {
            student.Gen_year(x);
        }
        public void select_students(DataGridView dgv, string clas, string year, string hf, ComboBox id_stu)
        {
            if (string.IsNullOrEmpty(clas) || string.IsNullOrEmpty(year)) { MessageBox.Show("Lỗi"); return; }
            Context.Select_Students(dgv, clas, year, hf, id_stu);
        }
        public void up_score_stu_info(DataGridView x)
        {
            using (Context ct = new Context())
            {
                string classid = "", stuid = "";
                classid = x.CurrentRow.Cells["IdClass"].Value.ToString();
                stuid = x.CurrentRow.Cells["IDStudent"].Value.ToString();
                V_Score stu = ct.V_Score.Where(w => w.IdClass == classid && w.IDStudent == stuid).FirstOrDefault();
                if (string.IsNullOrWhiteSpace(stu.IdClass = x.CurrentRow.Cells["IdClass"].Value.ToString()))
                {
                    return;
                }
                if (string.IsNullOrWhiteSpace(stu.IDStudent = x.CurrentRow.Cells["IDStudent"].Value.ToString()))
                {
                    return;
                }
                if (string.IsNullOrWhiteSpace(stu.NameStudent = x.CurrentRow.Cells["NameStudent"].Value.ToString()))
                {
                    return;
                }
                if (string.IsNullOrWhiteSpace(stu.Semester = x.CurrentRow.Cells["Semester"].Value.ToString()))
                { return; }
                if (string.IsNullOrWhiteSpace((stu.ScoreMath = double.Parse(x.CurrentRow.Cells["ScoreMath"].Value.ToString())).ToString()))
                { return; }
                if (string.IsNullOrWhiteSpace((stu.ScoreLiterature = double.Parse(x.CurrentRow.Cells["ScoreLiterature"].Value.ToString())).ToString()))
                { return; }
                if (string.IsNullOrWhiteSpace((stu.ScoreEnglish = double.Parse(x.CurrentRow.Cells["ScoreEnglish"].Value.ToString())).ToString()))
                { return; }
                if (string.IsNullOrWhiteSpace((stu.Medium = double.Parse(x.CurrentRow.Cells["Medium"].Value.ToString())).ToString()))
                { return; }
                if (string.IsNullOrWhiteSpace(stu.LearningPower = x.CurrentRow.Cells["LearningPower"].Value.ToString()))
                { return; }
                if (string.IsNullOrWhiteSpace(stu.Conduct = x.CurrentRow.Cells["Conduct"].Value.ToString()))
                { return; }
                if (string.IsNullOrWhiteSpace(stu.Note = x.CurrentRow.Cells["Note"].Value.ToString()))
                { return; }
                if (string.IsNullOrWhiteSpace(stu.YearScore = x.CurrentRow.Cells["YearScore"].Value.ToString()))
                { return; }
                add_to_sql_up(ct, stu, stuid, classid);
            }
        }//update score for student
        void add_to_sql_up(Context ct, V_Score stu, string stuid, string classid)
        {
            Score info = new Score();
            info = ct.Scores.Where(w => w.IDStudent == stuid).FirstOrDefault();
            info.IDStudent = stu.IDStudent;
            info.IdClass = stu.IdClass;
            //info.IDScore = stu.IDScore;
            info.ScoreMath = stu.ScoreMath;
            info.ScoreEnglish = stu.ScoreEnglish;
            info.ScoreLiterature = stu.ScoreLiterature;
            info.Semester = stu.Semester;
            info.Medium = stu.Medium;
            info.Note = stu.Note;
            info.LearningPower = stu.LearningPower;
            info.Conduct = stu.Conduct;
            Context.add_stu_to_sql(ct);
        }//update to sql infor score
        public void substring(ComboBox x,ComboBox y, string id )
        {
           string year =  x.Text.Substring(5);
            year = year + "-"+(int.Parse(year)+1).ToString();
            string classone =     y.Text.Substring(0, 1);

            string classtwo=y.Text.Substring(1);
            classone = (int.Parse(classone) + 1).ToString() + classtwo;
            Student st = new Student();
            StudentClass stc = new StudentClass();
            Score sc = new Score();
            using (Context ct = new Context())
            {
                st = ct.Students.Where(w => w.IDStudent == id).FirstOrDefault();
                stc.IDStudent = st.IDStudent;
                stc.YearClass = year;
                stc.IdClass = classone;
                sc.IDScore = "1";
                sc.IdClass = classone;
                sc.IDStudent = st.IDStudent;
                sc.YearScore = year;
                sc.Semester = "Kì I";
                Context.add_student_class(ct,stc,sc);
            }
        }
        void add_to_sql_up_class(Context ct, StudentClass stu, string stuid, string classid)
        {
            StudentClass info = new StudentClass();
            info = ct.StudentClasses.Where(w => w.IDStudent == stuid).FirstOrDefault();
            info.IDStudent = stu.IDStudent;
            info.IdClass = stu.IdClass;
            //info.IDScore = stu.IDScore;
            info.YearClass = stu.YearClass;
            Context.add_stu_to_sql(ct);
        }//update score student after update
        
    }
}
