﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using MVC.Models;
using MVC.Validators;
using FluentValidation.Results;

namespace MVC.Controllers
{
    class Ctrl_Student
    {
        public void Gen_year(ComboBox x)
        {
            for (int i = 1980; i <= DateTime.Now.Year; i++)
            {
                x.Items.Add((i + "-" + (i + 1)));
            }
        }//add the school year to combobox
        public void cbb_classes(ComboBox x)
        {
            Context.cbb_classes(x);

        }//add list class to combobox
        public void select_students(DataGridView dgv, string clas, string year, ComboBox id_stu)
        {
            if (string.IsNullOrEmpty(clas) || string.IsNullOrEmpty(year)) { MessageBox.Show("Lỗi"); return; }
            StudentClass.Select_students(dgv, clas, year, id_stu);
        }//void select student
        public void add_student_to_class(BindingSource bs, DataGridView dgv)
        {
            dgv.DataSource = bs;
            bs.MoveLast();
        }//add infor student to class
        public void cre_stu_info(DataGridView x)
        {
            using (Context ct = new Context())
            {
                LsClass stu = new LsClass();

                //if(string.IsNullOrWhiteSpace(stu.IdClass = x.CurrentRow.Cells["IdClass"].Value.ToString()))
                //{ return; }
                //if (string.IsNullOrWhiteSpace(stu.IDStudent = x.CurrentRow.Cells["IDStudent"].Value.ToString()))
                //{ return; }
                //if (string.IsNullOrWhiteSpace(stu.NameStudent = x.CurrentRow.Cells["NameStudent"].Value.ToString()))
                //{ return; }
                //if (string.IsNullOrWhiteSpace((stu.DateStudent = Convert.ToDateTime(x.CurrentRow.Cells["DateStudent"].Value.ToString())).ToString()))
                //{ return; }
                //if (string.IsNullOrWhiteSpace(stu.Sex = x.CurrentRow.Cells["Sex"].Value.ToString()))
                //{ return; }
                //if (string.IsNullOrWhiteSpace(stu.AdressS = x.CurrentRow.Cells["AdressS"].Value.ToString()))
                //{ return; }
                //if (string.IsNullOrWhiteSpace(stu.NationS = x.CurrentRow.Cells["NationS"].Value.ToString()))
                //{ return; }
                //if (string.IsNullOrWhiteSpace(stu.YearClass = x.CurrentRow.Cells["YearClass"].Value.ToString()))
                //{ return; }



                stu.IdClass = Convert.ToString(x.CurrentRow.Cells["IdClass"].Value);
                stu.IDStudent = Convert.ToString(x.CurrentRow.Cells["IDStudent"].Value);
                stu.NameStudent = Convert.ToString(x.CurrentRow.Cells["NameStudent"].Value);
                stu.DateStudent = Convert.ToDateTime(x.CurrentRow.Cells["DateStudent"].Value);
                stu.Sex = Convert.ToString(x.CurrentRow.Cells["Sex"].Value);
                stu.AdressS = Convert.ToString(x.CurrentRow.Cells["AdressS"].Value);
                stu.NationS = Convert.ToString(x.CurrentRow.Cells["NationS"].Value);
                stu.YearClass = Convert.ToString( x.CurrentRow.Cells["YearClass"].Value);

                StudentValidation Validators = new StudentValidation();
                ValidationResult result = Validators.Validate(stu);
                if (result.IsValid == false)
                {
                    foreach (ValidationFailure failture in result.Errors)
                    {
                        MessageBox.Show($"{failture.PropertyName}:{failture.ErrorMessage}");
                    }

                }
                else
                {
                    add_to_sql(ct, stu);
                }
              
            }
        }//create infor student

        public void up_stu_info(DataGridView x)
        {
            using (Context ct = new Context())
            {
                string classid = "", stuid = "";
                classid = x.CurrentRow.Cells["IdClass"].Value.ToString();
                stuid = x.CurrentRow.Cells["IDStudent"].Value.ToString();
                LsClass stu = ct.LsClasses.Where(w => w.IdClass == classid && w.IDStudent == stuid).FirstOrDefault();
                if (string.IsNullOrWhiteSpace(stu.IdClass = x.CurrentRow.Cells["IdClass"].Value.ToString()))
                {
                    return;
                }
                if (string.IsNullOrWhiteSpace(stu.IDStudent = x.CurrentRow.Cells["IDStudent"].Value.ToString()))
                {
                    return;
                }
                if (string.IsNullOrWhiteSpace(stu.NameStudent = x.CurrentRow.Cells["NameStudent"].Value.ToString()))
                {
                    return;
                }
                if (string.IsNullOrWhiteSpace((stu.DateStudent = Convert.ToDateTime(x.CurrentRow.Cells["DateStudent"].Value.ToString())).ToString()))
                {
                    return;
                }
                if (string.IsNullOrWhiteSpace(stu.Sex = x.CurrentRow.Cells["Sex"].Value.ToString()))
                {
                    return;
                }
                if (string.IsNullOrWhiteSpace(stu.AdressS = x.CurrentRow.Cells["AdressS"].Value.ToString()))
                {
                    return;
                }
                if (string.IsNullOrWhiteSpace(stu.NationS = x.CurrentRow.Cells["NationS"].Value.ToString()))
                {
                    return;
                }
                if (string.IsNullOrWhiteSpace(stu.YearClass = x.CurrentRow.Cells["YearClass"].Value.ToString()))
                {
                    return;
                }
                add_to_sql_up(ct, stu,stuid,classid);
            }
        }//update infor student
            void add_to_sql(Context ct, LsClass stu)
        {
            Student info = new Student();
            info.NationS = stu.NationS;
            info.AdressS = stu.AdressS;
            info.Sex = stu.Sex;
            info.IDStudent = stu.IDStudent;
            info.DateStudent = stu.DateStudent;
            info.NameStudent = stu.NameStudent;
            StudentClass a = new StudentClass();
            a.IDStudent = info.IDStudent;
            a.IdClass = stu.IdClass;
            a.YearClass = stu.YearClass;
           
              
                Context.add_stu_to_sql(ct, info, a, 1);
            
                //Context.add_stu_to_sql(ct, info, a, 1);
        }//save infor student to sql
        void add_to_sql_up(Context ct, LsClass stu, string stuid, string classid)
        {
            Student info = new Student();
            info = ct.Students.Where(w => w.IDStudent == stuid).FirstOrDefault();
            info.NationS = stu.NationS;
            info.AdressS = stu.AdressS;
            info.Sex = stu.Sex;
            info.IDStudent = stu.IDStudent;
            info.DateStudent = stu.DateStudent;
            info.NameStudent = stu.NameStudent;
            Context.add_stu_to_sql(ct);
        }//save update infor student for sql
        public void del_stu(DataGridView x)
        {
            if (MessageBox.Show("", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Context.del_stu(x);
            }
        }//delete student in class
    }
}
