﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MVC.Models;
using FluentValidation.Results;
using MVC.Validators;

namespace MVC.Controllers
{
    class Ctrl_GV
    {
        public void btn_change_status(Control x)
        {
            x.Enabled = !x.Enabled;
        }//update change button
        public void _insert_teacher(Teacher gv)
        {
           
            Teacher.insert_GV(gv);
        }//add infor teacher to data
        public void _update_teacher(Teacher gv,Context ct)
        {
            string notice = "Còn thiếu:";
            
            if (notice != "Còn thiếu:")
            {
                MessageBox.Show(notice, "Nhắc", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                Form1.error = 1;
                return;
            }
            
                Teacher.update_GV(ct);
            
        }//update infor teacher to data
        public void _del_teacher(Context ct, Teacher gv)
        {

            ct.Entry(gv).State = EntityState.Deleted;
            Teacher.del_teacher(ct);
        }//delete infor teacher to data
        public void un_del(List<Teacher> l)
        {
            using (Context ct = new Context())
            {
                ct.Teachers.AddRange(l);
                Teacher.un_del_teacher(ct);
            }
        }// undelete after delete teacher
    }
}
