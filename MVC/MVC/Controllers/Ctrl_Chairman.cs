﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MVC.Models;

namespace MVC.Controllers
{
    class Ctrl_Chairman
    {
        Ctrl_Student st = new Ctrl_Student();
             
        Ctrl_GV tc = new Ctrl_GV();
        public void cmb_classs(ComboBox x)
        {
            st.cbb_classes(x);
        }//add list class to combobox
        public void cmb_year(ComboBox x)
        {
            st.Gen_year(x);
        }//add the school year to combobox
        public void cbb_tc(ComboBox x)
        {
            Context.cbb_teacher(x);

        }//add id teacher to combobox
        public void text_tc(TextBox x)
        {
            Context.txt_tc(x);

        }//add infor teacher to textbox
        public void _insert_teacher(string id, string idcl, string year)
        {
            string notice = "Còn thiếu:";

            if (notice != "Còn thiếu:")
            {
                MessageBox.Show(notice, "Nhắc", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                Form1.error = 1;
                return;
            }
           
            
            using (Context ct = new Context())
            {
                string tmp = "";
                string l_Idcm = (ct.Chairnams.OrderByDescending(o => o.IdCm).Select(s => s.IdCm).First()).ToString();
                if ((int.Parse((l_Idcm.Substring(2, l_Idcm.Replace("CN", "").Length))) + 1) < 100)
                {
                    tmp = "0" + (int.Parse((l_Idcm.Substring(2, l_Idcm.Replace("CN", "").Length))) + 1).ToString();
                }
                l_Idcm = l_Idcm.Substring(0, 2)+ tmp;
                Chairnam cm = new Chairnam()
                {
                IdCm = l_Idcm,
                IdTc = id,
                IdClass = idcl,
                YearClass = year
            };
                Chairnam.insert_Chairman(cm,ct);
            }
        }//add infor teacher 
        public void add_chair_to_sql_up(string idtc,string idcl,string year)
        {
            Chairnam ch = new Chairnam();
            using (Context ct = new Context())
            {
                ch = ct.Chairnams.Where(w => w.IdTc == idtc).FirstOrDefault();
                ch.IdClass = idcl;
                ch.YearClass = year;
               Chairnam.add_chair_to_sql(ct);
            }
                

             
        }//update infor teacher
        public void select_chair(DataGridView dgv, string idtc, string clas, string year)
        {
            if (string.IsNullOrEmpty(clas) || string.IsNullOrEmpty(year) || string.IsNullOrEmpty(idtc))  { MessageBox.Show("Lỗi"); return; }
            Chairnam.select_chairmans(dgv, clas,idtc,year);
        }//add infor chairman to textbox and combobox
    }
}
