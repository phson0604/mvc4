﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MVC
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
            Application.Run(new Views.Students());
            //Application.Run(new Views.Score());
            //Application.Run(new Views.Main());
            //Application.Run(new Views.Chairman());
            //Application.Run(new Views.GV());

        }
    }
}
