﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Moq;
using MVC.Controllers;
using MVC.Views;
using MVC.Models;


namespace MVC.xUnit
{
    public class TestTC
    {

        [Fact]
        public void Tc()
        {
            string name = "hung";
            Ctrl_GV na = new Ctrl_GV();
            Teacher gv = new Teacher { IdTc = "GV0018", TeacherName = "hung", Sex = "Nam", PhoneTC = "0211541541", AdressTC = "hai phong", TeacherDate = DateTime.Parse("08/03/1990") };
            na._insert_teacher(gv);
            Assert.Equal(name, gv.TeacherName);


        }

        //[Fact]
        [Theory]
        [InlineData("GV0018","hung", "Nam", "0211541541", "hai phong", "08/03/1990")]
        public void Tc1(string IdTC,string TeacherName,string Sex,string PhoneTC,string AdressTC,DateTime TeacherDate)
        {
            Teacher gv = new Teacher();
            List<Teacher> ga = new List<Teacher>();
            ga.Add(gv);
            Assert.True(ga.Count == 1);
            Assert.Contains<Teacher>(gv, ga);
        }
       
        

    }
}
