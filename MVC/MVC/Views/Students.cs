﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MVC.Models;
using MVC.Controllers;
using MVC.Validators;
using FluentValidation;
using FluentValidation.Results;

namespace MVC.Views
{
    public partial class Students : Form
    {
        Ctrl_Student ctrl = new Ctrl_Student();
        public Students()
        {
            InitializeComponent();
        }

        private void Student_Load(object sender, EventArgs e)
        {
            ctrl.Gen_year(cmbYearClass);
            ctrl.cbb_classes(cmbClass);
            ctrl.cbb_classes(cmbTo);
        }

        private void dgrListStudent_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        //Search list student in class
        private void btnSearchClass_Click(object sender, EventArgs e)
        {
            ctrl.select_students(dgrListStudent, cmbClass.Text, cmbYearClass.Text, cmbIdS);
            bindingSource1.DataSource = dgrListStudent.DataSource;
        }

        //Add student 
        int add = 0;
        private void btnAdd_Click(object sender, EventArgs e)
        {
            
            ctrl.add_student_to_class(bindingSource1,dgrListStudent);

            if (add == 1)
            {
               
                ctrl.cre_stu_info(dgrListStudent);
               

                add = 2;
            }
            if(add==0)
            {
                add = 1;
            }
            if(add==2)
            {
                add = 0;
            }
        }

        //Update infor student
        int update = 0;
        private void btnRepair_Click(object sender, EventArgs e)
        {
            if (update == 1)
            {
                ctrl.up_stu_info(dgrListStudent);
                update = 2;
            }
            if (update == 0)
            {
                update = 1;
            }
            if (update == 2)
            {
                update = 0;
            }
        }

        //Delete infor student
        private void btnDel_Click(object sender, EventArgs e)
        {
            ctrl.del_stu(dgrListStudent);
        }
        private void cmbIdS_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
