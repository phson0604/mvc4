﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MVC.Models;
using MVC.Controllers;

namespace MVC.Views
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }
        
        //Void menu 
        private void MNTeacherToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool KT = false;
            foreach (Form form in Application.OpenForms.OfType<Form>().ToList())
            {
                if (form.GetType() == typeof(Form1))
                {

                    form.StartPosition = FormStartPosition.CenterScreen;
                    form.WindowState = FormWindowState.Maximized;
                    form.MdiParent = this;
                    form.Activate();
                    //phutrach.load();
                    KT = true;
                }
            }

            if (!KT)
            {
                Form1 HT = new Form1();
                //HT.StartPosition = FormStartPosition.CenterScreen;
                ////HT.WindowState = FormWindowState.Maximized;
                ////HT.MdiParent = this;
                ////this.Close();
                HT.Show();
            }
        }

        private void mnStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool KT = false;
            foreach (Form form in Application.OpenForms.OfType<Form>().ToList())
            {
                if (form.GetType() == typeof(Students))
                {

                    form.StartPosition = FormStartPosition.CenterScreen;
                    form.WindowState = FormWindowState.Maximized;
                    form.MdiParent = this;
                    form.Activate();
                    //phutrach.load();
                    KT = true;
                }
            }

            if (!KT)
            {
                Students HT = new Students();
                //HT.StartPosition = FormStartPosition.CenterScreen;
                ////HT.WindowState = FormWindowState.Maximized;
                ////HT.MdiParent = this;
                ////this.Close();
                HT.Show();
            }
        }

        private void mnScoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool KT = false;
            foreach (Form form in Application.OpenForms.OfType<Form>().ToList())
            {
                if (form.GetType() == typeof(Score))
                {

                    form.StartPosition = FormStartPosition.CenterScreen;
                    form.WindowState = FormWindowState.Maximized;
                    form.MdiParent = this;
                    form.Activate();
                    //phutrach.load();
                    KT = true;
                }
            }

            if (!KT)
            {
                Score HT = new Score();
                //HT.StartPosition = FormStartPosition.CenterScreen;
                ////HT.WindowState = FormWindowState.Maximized;
                ////HT.MdiParent = this;
                ////this.Close();
                HT.Show();
            }
        }

      

        private void ChairmanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool KT = false;
            foreach (Form form in Application.OpenForms.OfType<Form>().ToList())
            {
                if (form.GetType() == typeof(Chairman))
                {

                    form.StartPosition = FormStartPosition.CenterScreen;
                    form.WindowState = FormWindowState.Maximized;
                    form.MdiParent = this;
                    form.Activate();
                    //phutrach.load();
                    KT = true;
                }
            }

            if (!KT)
            {
                Chairman HT = new Chairman();
                //HT.StartPosition = FormStartPosition.CenterScreen;
                ////HT.WindowState = FormWindowState.Maximized;
                ////HT.MdiParent = this;
                ////this.Close();
                HT.Show();
            }
        }

        private void exitToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn chắc chắn muốn thoát?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                Application.Exit();
        }

        private void scoreToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
