﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MVC.Models;
using MVC.Controllers;

namespace MVC.Views
{
    public partial class Chairman : Form
    {
        Ctrl_Chairman ctrl = new Ctrl_Chairman();
        int add = 0;
        int update = 0;
        public Chairman()
        {
            InitializeComponent();
        }

        //Load form
        private void Chairman_Load(object sender, EventArgs e)
        {
            ctrl.text_tc(txtNameTeacher);
            ctrl.cmb_classs(cbIdClass);
            ctrl.cmb_year(cbIdYear);
            ctrl.cbb_tc(cbIdTeacher);
            Chairnam.Select_Chairman(dgvChairman);
            ctrl.text_tc(txtNameTeacher);
        }

        //button add
        private void btnADD_Click(object sender, EventArgs e)
        {
            add = 1;
        }
        //button save after add
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (add == 1)
            {
                ctrl._insert_teacher(cbIdTeacher.Text,cbIdClass.Text,cbIdYear.Text );
              
            }
            if(update==1)
            {
                ctrl.add_chair_to_sql_up(cbIdTeacher.Text, cbIdClass.Text, cbIdYear.Text);
                
            }
        }
        //load infor to textbox
        private void txtNameTeacher_KeyDown(object sender, KeyEventArgs e)
        {
           if(e.KeyCode == Keys.Enter)
            {
                try
                {
                    cbIdTeacher.Text = txtNameTeacher.Text.Substring(txtNameTeacher.Text.IndexOf('-')+1);
             
                    txtNameTeacher.Text = txtNameTeacher.Text.Remove(txtNameTeacher.Text.IndexOf('-'));

                }
                catch { }

            }
        }

        private void txtNameTeacher_TextChanged(object sender, EventArgs e)
        {
            bool a = false;
            try
            {
               a  = char.IsNumber(txtNameTeacher.Text.Substring(txtNameTeacher.Text.IndexOf('-') + 2)[0]);
            }
            catch { return; }
                if (a)
                {
                    try
                    {
                        cbIdTeacher.Text = txtNameTeacher.Text.Substring(txtNameTeacher.Text.IndexOf('-') + 1);

                        txtNameTeacher.Text = txtNameTeacher.Text.Remove(txtNameTeacher.Text.IndexOf('-'));

                    }
                    catch { }
                }
            }
        //Button update
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            update = 1;
        }

        private void dgvChairman_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            cbIdTeacher.Text = dgvChairman.CurrentRow.Cells["IdTc"].Value.ToString();
            cbIdClass.Text = dgvChairman.CurrentRow.Cells["IdClass"].Value.ToString();
            cbIdYear.Text = dgvChairman.CurrentRow.Cells["YearClass"].Value.ToString();
            txtNameTeacher.Text = dgvChairman.CurrentRow.Cells["NameTC"].Value.ToString();
        }
        //Button search
        private void btnSearch_Click(object sender, EventArgs e)
        {
            ctrl.select_chair(dgvChairman, cbIdTeacher.Text,cbIdClass.Text, cbIdYear.Text);
            
            
        }
    }
   }
