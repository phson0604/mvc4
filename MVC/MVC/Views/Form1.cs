﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MVC.Models;
using MVC.Controllers;
namespace MVC
{
    public partial class Form1 : Form
    {
        Ctrl_GV ctrl = new Ctrl_GV();
        int add = 0;
        int update = 0;
        int delete = 0;
        public static int error = 0;
        List<Teacher> del = new List<Teacher>();
        public Form1()
        {
            InitializeComponent();
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.WindowState = FormWindowState.Maximized;
        }
        #region event

        private void Form1_Load(object sender, EventArgs e)
        {
            _Default_setting();
        }

        // delete teacher
        private void button5_Click(object sender, EventArgs e)
        {
            if(delete==1)
            {
                ctrl.un_del(del);
            }
            _Default_setting();
        }

        //Add teacher
        private void button1_Click(object sender, EventArgs e)
        {
            ctrl.btn_change_status(button2);
            ctrl.btn_change_status(button3);
            ctrl.btn_change_status(button4);
            ctrl.btn_change_status(button1);
            label7.Text = ("Đang thêm").ToUpper();
            add = 1;
        }
        
        //Save after add,update,delete
        private void button4_Click(object sender, EventArgs e)
        {
            if(add==1)
            {
                Teacher gv = new Teacher()
                {
                    IdTc = txtid.Text,
                    TeacherName = txtname.Text,
                    Sex = cbbgender.Text,
                    TeacherDate = dtpdob.Value,
                    AdressTC =txtaddress.Text,
                    PhoneTC =txtphone.Text 
                };
                ctrl._insert_teacher(gv);

                if (error == 1)
                {
                    error = 0;
                    return;
                }
                _Default_setting();
            }
            if(update==1)
            {
                Teacher gv = new Teacher();

                using (Context ct = new Context())
                {
                    string id = txtid.Text;

                    gv = ct.Teachers.Where(w => w.IdTc == id).FirstOrDefault();
                    gv.TeacherName = txtname.Text;
                    gv.TeacherDate = dtpdob.Value;
                    gv.Sex = cbbgender.Text;
                    gv.AdressTC =txtaddress.Text;
                    gv.PhoneTC = txtphone.Text;

                    ctrl._update_teacher(gv,ct);
                }
                    if (error == 1)
                        
                {
                    error = 0;
                    return;
                }
                _Default_setting();
            }
            if(delete==1)
            {
                _Default_setting();
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtid.Text = dataGridView1.CurrentRow.Cells["IdTc"].Value.ToString();
            txtname.Text = dataGridView1.CurrentRow.Cells["TeacherName"].Value.ToString();
            txtaddress.Text = dataGridView1.CurrentRow.Cells["AdressTC"].Value.ToString();
            txtphone.Text = dataGridView1.CurrentRow.Cells["PhoneTC"].Value.ToString();
            dtpdob.Text = dataGridView1.CurrentRow.Cells["TeacherDate"].Value.ToString();
            cbbgender.Text = dataGridView1.CurrentRow.Cells["Sex"].Value.ToString();
        }

        //button update
        private void button2_Click(object sender, EventArgs e)
        {
            update = 1;
            ctrl.btn_change_status(button2);
            ctrl.btn_change_status(button3);
            ctrl.btn_change_status(button4);
            ctrl.btn_change_status(button1);
            label7.Text = "Đang sửa".ToUpper();

        }

        //button delete

        private void button3_Click(object sender, EventArgs e)
        {
            ctrl.btn_change_status(button2);
            ctrl.btn_change_status(button3);
            ctrl.btn_change_status(button4);
            ctrl.btn_change_status(button1);
            label7.Text = ("Đang xóa").ToUpper();
            delete = 1;
        }

        //delete by double click
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (delete == 1)
            {
                string id = dataGridView1.CurrentRow.Cells["IdTc"].Value.ToString();
                if (MessageBox.Show("Xác nhận xóa:\n" + id + "-" + dataGridView1.CurrentRow.Cells["TeacherName"].Value.ToString(), "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    using (Context ct = new Context())
                    {
                        Teacher gv = ct.Teachers.Where(w => w.IdTc == id).FirstOrDefault();
                        del.Add(gv);
                        ctrl._del_teacher(ct, gv);
                        load();
                    }
                }
            }
        }

        //Rxit form
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (delete == 1)
                if (MessageBox.Show("có những thay đổi chưa lưu bạn có muốn lưu không?", "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    _Default_setting();
                }
                else
                {
                    button5.PerformClick();
                }
        }

        #endregion
        #region method

        //Load form
        public void _Default_setting()
        {
            foreach (Control item in tableLayoutPanel2.Controls)
            {
                if (!item.Enabled && item.GetType() == typeof(TextBox))
                {
                    ctrl.btn_change_status(item);
                }
            }
            txtid.ResetText();
            txtaddress.ResetText();
            txtname.ResetText();
            txtphone.ResetText();
            cbbgender.ResetText();
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
            button4.Enabled = false;
            button5.Enabled = true;
            load();
            add = 0;
            update = 0;
            delete = 0;
            label7.ResetText();
            del.RemoveRange(0, del.Count);
        }
        void load()
        {
            Teacher.Select_GV(dataGridView1);
            //del.RemoveRange(0,del.Count);
        }
        #endregion
    }
}
