namespace MVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LearningPower")]
    public partial class LearningPower
    {
        [Key]
        public int IDLP { get; set; }

        [StringLength(20)]
        public string NameLP { get; set; }
    }
}
