namespace MVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Student")]
    public partial class Student
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Student()
        {
            Scores = new HashSet<Score>();
            StudentClasses = new HashSet<StudentClass>();
        }

        [Key]
        [StringLength(10)]
        public string IDStudent { get; set; }

        [StringLength(50)]
        public string NameStudent { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateStudent { get; set; }

        [StringLength(100)]
        public string AdressS { get; set; }

        [StringLength(25)]
        public string Sex { get; set; }

        [StringLength(25)]
        public string NationS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Score> Scores { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StudentClass> StudentClasses { get; set; }
    }
}
