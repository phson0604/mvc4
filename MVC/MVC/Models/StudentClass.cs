namespace MVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Data.Entity;

    using System.Linq;
    using System.Windows.Forms;

    [Table("StudentClass")]
    public partial class StudentClass
    {
        [Key]
        public int IdStudentClass { get; set; }

        [StringLength(10)]
        public string IDStudent { get; set; }

        [StringLength(10)]
        public string IdClass { get; set; }

        [StringLength(20)]
        public string YearClass { get; set; }

        public virtual Class Class { get; set; }

        public virtual Student Student { get; set; }

        public static void Select_students(DataGridView dgv, string clas, string year, ComboBox id_stu)
        {
            using (Context ct = new Context())
            {
                dgv.DataSource = ct.LsClasses.Where(w => w.IdClass == clas && w.YearClass == year).ToList();
                id_stu.DataSource = ct.LsClasses.Where(w => w.IdClass == clas && w.YearClass == year).ToList();
                id_stu.DisplayMember = "IDStudent";
                id_stu.ValueMember = "IDStudent";
            }
        }

        //    public static void add_stu_to_sql(Context ct, Student info, StudentClass a, int add)
        //    {
        //        if (add == 1)
        //        {
        //            ct.Students.Add(info);
        //            ct.StudentClasses.Add(a);
        //            ct.SaveChanges();
        //        }
        //    }

        //    public static void add_stu_to_sql(Context ct)
        //    {
        //        ct.SaveChanges();
        //    }
        //}
    }
}
