namespace MVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Account")]
    public partial class Account
    {
        [Key]
        [StringLength(20)]
        public string Username { get; set; }

        [StringLength(20)]
        public string Pass { get; set; }

        [StringLength(50)]
        public string access { get; set; }
    }
}
