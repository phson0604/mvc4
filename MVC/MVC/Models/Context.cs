namespace MVC.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Windows.Forms;

    public partial class Context : DbContext
    {
        public Context()
            : base("name=Context")
        {
        }

        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Chairnam> Chairnams { get; set; }
        public virtual DbSet<Class> Classes { get; set; }
        public virtual DbSet<LearningPower> LearningPowers { get; set; }
        public virtual DbSet<Score> Scores { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<StudentClass> StudentClasses { get; set; }
        public virtual DbSet<Teacher> Teachers { get; set; }
        public virtual DbSet<YearClass> YearClasses { get; set; }
        public virtual DbSet<C_tc> C_tc { get; set; }
        public virtual DbSet<InputScore> InputScores { get; set; }
        public virtual DbSet<LsClass> LsClasses { get; set; }
        public virtual DbSet<V_Score> V_Score { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                .Property(e => e.Username)
                .IsUnicode(false);

            modelBuilder.Entity<Account>()
                .Property(e => e.Pass)
                .IsUnicode(false);

            modelBuilder.Entity<Chairnam>()
                .Property(e => e.IdCm)
                .IsUnicode(false);

            modelBuilder.Entity<Chairnam>()
                .Property(e => e.IdTc)
                .IsUnicode(false);

            modelBuilder.Entity<Chairnam>()
                .Property(e => e.IdClass)
                .IsUnicode(false);

            modelBuilder.Entity<Class>()
                .Property(e => e.IdClass)
                .IsUnicode(false);

            modelBuilder.Entity<Score>()
                .Property(e => e.IDScore)
                .IsUnicode(false);

            modelBuilder.Entity<Score>()
                .Property(e => e.IDStudent)
                .IsUnicode(false);

            modelBuilder.Entity<Score>()
                .Property(e => e.IdClass)
                .IsUnicode(false);

            modelBuilder.Entity<Student>()
                .Property(e => e.IDStudent)
                .IsUnicode(false);

            modelBuilder.Entity<StudentClass>()
                .Property(e => e.IDStudent)
                .IsUnicode(false);

            modelBuilder.Entity<StudentClass>()
                .Property(e => e.IdClass)
                .IsUnicode(false);

            modelBuilder.Entity<Teacher>()
                .Property(e => e.IdTc)
                .IsUnicode(false);

            modelBuilder.Entity<Teacher>()
                .Property(e => e.PhoneTC)
                .IsUnicode(false);

            modelBuilder.Entity<C_tc>()
                .Property(e => e.IdTc)
                .IsUnicode(false);

            modelBuilder.Entity<C_tc>()
                .Property(e => e.PhoneTC)
                .IsUnicode(false);

            modelBuilder.Entity<InputScore>()
                .Property(e => e.IDScore)
                .IsUnicode(false);

            modelBuilder.Entity<InputScore>()
                .Property(e => e.IDStudent)
                .IsUnicode(false);

            modelBuilder.Entity<InputScore>()
                .Property(e => e.IdClass)
                .IsUnicode(false);

            modelBuilder.Entity<LsClass>()
                .Property(e => e.IdClass)
                .IsUnicode(false);

            modelBuilder.Entity<LsClass>()
                .Property(e => e.IDStudent)
                .IsUnicode(false);

            modelBuilder.Entity<V_Score>()
                .Property(e => e.IdClass)
                .IsUnicode(false);

            modelBuilder.Entity<V_Score>()
                .Property(e => e.IDStudent)
                .IsUnicode(false);
        }
        //public static void Select_Chairman(DataGridView dgv)
        //{
        //    using (Context ct = new Context())
        //    {
        //        dgv.DataSource = ct.Chairnams.ToList();
        //        dgv.Columns.Remove("Class");
        //        dgv.Columns.Remove("Teacher");
        //        DataGridViewTextBoxColumn col = new DataGridViewTextBoxColumn();
        //        col.Name = "NameTC";
        //        col.HeaderText = "Name";
        //        dgv.Columns.Add(col);
        //        dgv.Columns[col.Name].DisplayIndex = 2;
        //        for (int i = 0; i < dgv.Rows.Count; i++)
        //        {
        //            string tmp = dgv.Rows[i].Cells["IdTc"].Value.ToString();
        //            var tc = ct.Teachers.Where(w => w.IdTc == tmp).FirstOrDefault();
        //            dgv.Rows[i].Cells[col.Name].Value = tc.TeacherName;
        //        }
        //    }
        //}

        //public static void Select_GV(DataGridView dgv)
        //{
        //    using (Context ct = new Context())
        //    {
        //        dgv.DataSource = ct.C_tc.ToList();
        //    }
        //}
        //public static void insert_GV(Teacher gv)
        //{
        //    using (Context ct = new Context())
        //    {
        //        ct.Teachers.Add(gv);
        //        ct.SaveChanges();
        //    }
        //}
        //public static void insert_Chairman(Chairnam cm, Context ct)
        //{
        //        string l_Idcm = (ct.Chairnams.OrderByDescending(o => o.IdCm).Select(s=>s.IdCm).First()).ToString();
        //        ct.Chairnams.Add(cm);
        //        ct.SaveChanges();
        //}
        //public static void update_GV(Context ct)
        //{
        //    ct.SaveChanges();
        //}
        //public static void del_teacher(Context ct)
        //{
        //    ct.SaveChanges();
        //}
        //public static void un_del_teacher(Context ct)
        //{
        //    ct.SaveChanges();
        //}
        public static void cbb_classes(ComboBox x)
        {
            using (Context ct = new Context())
            {
                x.DataSource = ct.Classes.ToList();
                x.DisplayMember = "IdClass";
                x.ValueMember = "IdClass";
            }
        }//display id in combobox
        public static void txt_tc(TextBox x)
        {
            
            using (Context ct = new Context())
            {
                    x.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    x.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    AutoCompleteStringCollection acsc = new AutoCompleteStringCollection();
                string[] name = ct.Teachers.Select(s => s.TeacherName).ToArray();
                string[] id = ct.Teachers.Select(s => s.IdTc).ToArray();
                for (int i = 0; i < id.Length; i++)
                {
                    acsc.Add(name[i]+"-"+id[i]);
                }
                x.AutoCompleteCustomSource = acsc;
            }
        }
        public static void cbb_teacher(ComboBox x)
        {
            using (Context ct = new Context())
            {
                x.DataSource = ct.Teachers.ToList();
                x.DisplayMember = "IdTc";
                x.ValueMember = "IdTc";
            }
        }//display id teacher in combobox
        //public static void Select_students(DataGridView dgv, string clas, string year, ComboBox id_stu)
        //{
        //    using (Context ct = new Context())
        //    {
        //        dgv.DataSource = ct.LsClasses.Where(w => w.IdClass == clas && w.YearClass == year).ToList();
        //        id_stu.DataSource = ct.LsClasses.Where(w => w.IdClass == clas && w.YearClass == year).ToList();
        //        id_stu.DisplayMember = "IDStudent";
        //        id_stu.ValueMember = "IDStudent";
        //    }
        //}
        public static void add_stu_to_sql(Context ct, Student info, StudentClass a, int add)
        {
            if (add == 1)
            {
                ct.Students.Add(info);
                ct.StudentClasses.Add(a);
                ct.SaveChanges();
            }
        }//update sql after add infor student
        public static void add_stu_to_sql(Context ct)
        {
            ct.SaveChanges();
        }
        //public static void add_chair_to_sql(Context ct)
        //{
        //    ct.SaveChanges();
        //}//update sql after add infor chair
        public static void del_stu(DataGridView x)
        {
            using (Context ct = new Context())
            {
                string classid = "", stuid = "";
                classid = x.CurrentRow.Cells["IdClass"].Value.ToString();
                stuid = x.CurrentRow.Cells["IDStudent"].Value.ToString();
                object stu = ct.StudentClasses.Where(w => w.IDStudent == stuid && w.IdClass == classid).FirstOrDefault();
                ct.Entry(stu).State = EntityState.Deleted;
                ct.SaveChanges();
            }
        }
        public static void Select_Students(DataGridView dgv, string clas, string year,string hf, ComboBox id_stu)
        {
            using (Context ct = new Context())
            {
                dgv.DataSource = ct.V_Score.Where(w => w.IdClass == clas && w.YearScore == year && w.Semester == hf).ToList();
                id_stu.DataSource = ct.V_Score.Where(w => w.IdClass == clas && w.YearScore == year && w.Semester == hf).ToList();
                id_stu.DisplayMember = "IDStudent";
                id_stu.ValueMember = "IDStudent";
            }
        }
        //public static void select_chairmans(DataGridView dgv,string idcl,string idtc,string year)
        //{
        //    using (Context xt = new Context())
        //    {
               
        //        dgv.DataSource = xt.Chairnams.Where(w=>w.IdTc==idtc&& w.IdClass==idcl&&w.YearClass==year).ToList();
        //        dgv.Columns.Remove("Class");
        //        dgv.Columns.Remove("Teacher");
        //        for(int i =0; i<dgv.RowCount; i++)
        //        {
        //            string tmp = dgv.Rows[i].Cells["IdTc"].Value.ToString();
        //            var ch = xt.Teachers.Where(w => w.IdTc == idtc).FirstOrDefault();
        //            dgv.Rows[i].Cells["NameTC"].Value = ch.TeacherName;
        //        }

        //    }
        //}
        public static void add_score_to_sql(Context ct, Score info,  int add)
        {
            if (add == 1)
            {
                ct.Scores.Add(info);       
                ct.SaveChanges();
            }
        }//update sql after add score
        public static void add_student_add_to_sql(Context ct, StudentClass info, int add)
        {
            if (add == 1)
            {
                ct.StudentClasses.Add(info);
                ct.SaveChanges();
            }
        }//add infor student to class in sql
        public static void add_student_class(Context ct,StudentClass info ,Score sc)
        {       
            //ct.StudentClasses.Add(info);
            ct.Scores.Add(sc);
            ct.SaveChanges();
            
        }
    }
}
