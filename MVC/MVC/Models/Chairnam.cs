namespace MVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    using System.Data.Entity;
   
    using System.Linq;
    using System.Windows.Forms;
    [Table("Chairnam")]
    public partial class Chairnam
    {
        [Key]
        [StringLength(10)]
        public string IdCm { get; set; }

        [StringLength(20)]
        public string IdTc { get; set; }

        [StringLength(10)]
        public string IdClass { get; set; }

        [StringLength(20)]
        public string YearClass { get; set; }

        public virtual Class Class { get; set; }

        public virtual Teacher Teacher { get; set; }

        public static void Select_Chairman(DataGridView dgv)
        {
            using (Context ct = new Context())
            {
                dgv.DataSource = ct.Chairnams.ToList();
                dgv.Columns.Remove("Class");
                dgv.Columns.Remove("Teacher");
                DataGridViewTextBoxColumn col = new DataGridViewTextBoxColumn();
                col.Name = "NameTC";
                col.HeaderText = "Name";
                dgv.Columns.Add(col);
                dgv.Columns[col.Name].DisplayIndex = 2;
                for (int i = 0; i < dgv.Rows.Count; i++)
                {
                    string tmp = dgv.Rows[i].Cells["IdTc"].Value.ToString();
                    var tc = ct.Teachers.Where(w => w.IdTc == tmp).FirstOrDefault();
                    dgv.Rows[i].Cells[col.Name].Value = tc.TeacherName;
                }
            }
        }

        public static void insert_Chairman(Chairnam cm, Context ct)
        {
            string l_Idcm = (ct.Chairnams.OrderByDescending(o => o.IdCm).Select(s => s.IdCm).First()).ToString();
            ct.Chairnams.Add(cm);
            ct.SaveChanges();
        }

        public static void select_chairmans(DataGridView dgv, string idcl, string idtc, string year)
        {
            using (Context xt = new Context())
            {

                dgv.DataSource = xt.Chairnams.Where(w => w.IdTc == idtc && w.IdClass == idcl && w.YearClass == year).ToList();
                dgv.Columns.Remove("Class");
                dgv.Columns.Remove("Teacher");
                for (int i = 0; i < dgv.RowCount; i++)
                {
                    string tmp = dgv.Rows[i].Cells["IdTc"].Value.ToString();
                    var ch = xt.Teachers.Where(w => w.IdTc == idtc).FirstOrDefault();
                    dgv.Rows[i].Cells["NameTC"].Value = ch.TeacherName;
                }

            }
        }

        public static void add_chair_to_sql(Context ct)
        {
            ct.SaveChanges();
        }
    }
   
}
