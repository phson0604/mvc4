namespace MVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Score")]
    public partial class Score
    {
        [Key]
        [StringLength(20)]
        public string IDScore { get; set; }

        [StringLength(10)]
        public string IDStudent { get; set; }

        [StringLength(10)]
        public string IdClass { get; set; }

        [StringLength(20)]
        public string YearScore { get; set; }

        [StringLength(20)]
        public string Semester { get; set; }

        public double? ScoreMath { get; set; }

        public double? ScoreLiterature { get; set; }

        public double? ScoreEnglish { get; set; }

        public double? Medium { get; set; }

        [StringLength(25)]
        public string LearningPower { get; set; }

        [StringLength(25)]
        public string Conduct { get; set; }

        [StringLength(500)]
        public string Note { get; set; }

        public virtual Class Class { get; set; }

        public virtual Student Student { get; set; }
    }
}
