namespace MVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LsClass")]
    public partial class LsClass
    {
        [StringLength(10)]
        public string IdClass { get; set; }

        [Key]
        [StringLength(10)]
        public string IDStudent { get; set; }

        [StringLength(50)]
        public string NameStudent { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateStudent { get; set; }

        [StringLength(100)]
        public string AdressS { get; set; }

        [StringLength(25)]
        public string Sex { get; set; }

        [StringLength(25)]
        public string NationS { get; set; }

        [StringLength(20)]
        public string YearClass { get; set; }
    }
}
