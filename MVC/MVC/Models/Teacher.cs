namespace MVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    using System.Data.Entity;

    using System.Linq;
    using System.Windows.Forms;
    //using FluentValidation;

    [Table("Teacher")]
    public partial class Teacher
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Teacher()
        {
            Chairnams = new HashSet<Chairnam>();
        }

        [Key]
        [StringLength(20)]
        public string IdTc { get; set; }

        [StringLength(50)]
        public string TeacherName { get; set; }

        [StringLength(25)]
        public string Sex { get; set; }

        [Column(TypeName = "date")]
        public DateTime TeacherDate { get; set; }

        [StringLength(50)]
        public string AdressTC { get; set; }

        [StringLength(20)]
        public string PhoneTC { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Chairnam> Chairnams { get; set; }

        //public class PersonValidator : AbstractValidator<Teacher>
        //{
        //    public PersonValidator()
        //    {
        //        RuleFor(p => p.TeacherName).NotEmpty().WithMessage("Teacher Name is required ");
        //        RuleFor(p => p.AdressTC).NotEmpty();
        //        RuleFor(p => p.PhoneTC).NotEmpty();
        //        RuleFor(p => p.Sex).NotEmpty();
        //        RuleFor(p => p.TeacherDate).NotEmpty();
        //    }
        //}

        public static void Select_GV(DataGridView dgv)
        {
            using (Context ct = new Context())
            {
                dgv.DataSource = ct.C_tc.ToList();
            }
        }

        public static void insert_GV(Teacher gv)
        {
            using (Context ct = new Context())
            {
                ct.Teachers.Add(gv);
                ct.SaveChanges();
            }
        }

        public static void update_GV(Context ct)
        {
            ct.SaveChanges();
        }
        public static void del_teacher(Context ct)
        {
            ct.SaveChanges();
        }
        public static void un_del_teacher(Context ct)
        {
            ct.SaveChanges();
        }
    }
}
