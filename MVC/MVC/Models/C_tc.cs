namespace MVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C_tc
    {
        [Key]
        [StringLength(20)]
        public string IdTc { get; set; }

        [StringLength(50)]
        public string TeacherName { get; set; }

        [StringLength(25)]
        public string Sex { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TeacherDate { get; set; }

        [StringLength(50)]
        public string AdressTC { get; set; }

        [StringLength(20)]
        public string PhoneTC { get; set; }
    }
}
